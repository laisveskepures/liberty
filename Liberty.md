# Liberty

2020-12-12

New motion in Lithuanian parliament to decriminalize cannabis got enough signatures to be considered. The promises by the "Laisves partija" have been made for the hearing to happen before the new year. It is absolutely wonderful to see such initiative. However if the bet had to be made, this is not going to pass. It will still be beneficial to see which parties/MPs are going to take their place on the wrong side of history by voting against. 